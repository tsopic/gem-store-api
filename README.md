# README #

This is example of using [Sails.JS](http://sailsjs.org/) as backend REST API for storing
Flatlander's Gem Store product and reviews data, more information about Flatlander's Gem store at [Sharping up with angular.js](https://www.codeschool.com/courses/shaping-up-with-angular-js).

Ready to go, Frontend application is located in other repository
https://bitbucket.org/tsopic/gem-store



### How to deploy the sails.js application? ###

* Dependencies
   -In order to run the application please fallow this tutorial http://sailsjs.org/#!/getStarted
   - Mysql 5.*
   - [NodeJs](https://nodejs.org/)

##Installing Sails.JS##
1. Open up terminal,
2. Make a directory, where sails will be installed
3. Execute these commands in command prompt to make sails directory and install sails

```
 #!command line
 mkdir Sails
 cd Sails
 npm -g install sails
```



##Configuring project ##
- Clone the project from bitbucket

```
#!command line

git clone git clone https://<username>@bitbucket.org/tsopic/gem-store-api.git
```
- Install sails-mysql which is required in order to use database for storing products/reviews

```
#!command line

npm install sails-mysql
```
- Create mysql user and Database.
Open up mysql shell, or use phpmyadmin, and execute fallowing sql commands, replace the IDENTIFIED BY 'password' value
with prefered database user password
```
#!SQL
CREATE USER 'root'@'localhost' IDENTIFIED BY 'password';
CREATE DATABASE GEMSTORE;
GRANT ALL PRIVILEGES ON GEMSTORE.* TO 'demoUser'@'localhost';
```

- Configure database
Config is located under - /config/connections.js
replace the values with your d

```
#!javascript
someMysqlServer: {
    adapter: 'sails-mysql',
    host: 'localhost',  // db host
    user: 'root', // db user
    password: 'password', //db password
    database: 'GEMSTORE' //database
  },
```

## Running API ##
Now just run the application and let the sails do rest of the magic.

```
#!Command line
sails lift
```
All the required database tables will be automaticly generated, and store backend is ready to go.

See the frontend project folder /json/product.js for product sample data
For testing you can use Chrome plugin - [postman](https://chrome.google.com/webstore/detail/postman-rest-client/) , post one of the products to
api url "http://loaclhost:1337/product" and see if it shows up on frontend application

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact