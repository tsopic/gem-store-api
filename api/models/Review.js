/**
* Review.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  attributes: {
    stars:{
      type: 'integer',
      max: 5,
      min: 1
    },
    body: {
      type: 'string'
    },
    author: {
      type: 'email'
    },
    product_review: {
      model : 'product'
    }
  }
};

